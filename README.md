# Sujet
- Source : http://exercicecorrige.blogspot.com/2013/08/ascenseur.html?spref=bl

Un produit va être installé pour contrôler N ascenseurs dans un gratte-ciel de M étages. Notre problème concerne la logique nécessaire au déplacement des ascenseurs entre les étages en accord avec les contraintes suivantes:

a. chaque ascenseur possède un ensemble de M boutons, un pour chaque étage. Un bouton s’allume lorsqu’il est appuyé et provoque le déplacement de l’ascenseur vers l’étage correspondant.

b. chaque étage, à l’exception du premier et du dernier, possède deux boutons, un pour demander la montée et un pour demander  la descente. Ces boutons s’allument lorsqu’ils sont appuyés. Ils s’éteignent quand l’ascenseur arrive à l’étage, et celui ci se déplace ensuite dans la direction demandée.

c. quand un ascenseur n’est pas requis, il reste à l’étage où il se trouve et ferme ses portes.

  

Décrire à l’aide d’un diagramme de séquence chacun des scénarios suivants:

1. requête d’ascenseur depuis l’étage  
2. requête d’étage depuis l’ascenseur